#include "transport/transport.hpp"
#include "proto/message.hpp"

#include <iostream>
#include <iomanip>
#include <cassert>


int main(int /*argc*/, char* /*argv*/[])
{
    // client doesn't send, so it's agent id isn't used
    constexpr auto agent_id = 0;
    auto bcaster = transport::make_broadcaster(agent_id);

    proto::msg m;
    auto ar = bcaster->make_iarchive(m.buf_size());

    // speed up C++ ostreams by disabling sync w/ C streams
    std::ios_base::sync_with_stdio(false);

    while (true)
    {
        try
        {
            ar->reset();
            auto rslt = bcaster->receive(*ar);
            namespace sch = std::chrono;
            auto rsv_tp = sch::system_clock::now();
            m.deserialize(*ar);
            auto snd_tp = m.timestamp();
            std::cout
                << "Agent " << std::setw(3) << std::left << unsigned(rslt.first)
                << "(" << rslt.second << ") latency: ";

            if (rsv_tp < snd_tp)
                std::cout << "negative (server and client clocks aren't syncronized)\n";
            else
                std::cout
                    << sch::duration_cast<sch::microseconds>(rsv_tp - snd_tp).count()
                    << " us\n";
            std::cout.flush();
        }
        catch (const std::exception& e)
        {
            std::cerr << "Unexpected error: " << e.what() << std::endl;
        }
    }
    return 0;
}
