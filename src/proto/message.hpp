#pragma once

#include "iface/archive.hpp"

#include <chrono>
#include <vector>
#include <variant>
#include <limits>

#include <cstdint>


namespace proto {

class msg
{
    using duration_type = std::chrono::duration<
        std::make_unsigned<std::chrono::system_clock::rep>::type,
        std::chrono::system_clock::period
    >;
    using time_point_type = std::chrono::time_point<
        std::chrono::system_clock,
        duration_type
    >;
    using timestamp_repr_type = std::uint64_t;

    // Timestamp representation valid range is: [0 - 2^54); about 570`000 years in microseconds
    // std::chrono::microseconds provide the same guarantee
    static constexpr timestamp_repr_type MAX_TP_US_VAL = 0x3F'FFFF'FFFF'FFFF;
    static_assert(
        static_cast<timestamp_repr_type>(
            static_cast<std::chrono::microseconds::rep>(MAX_TP_US_VAL)
        ) == MAX_TP_US_VAL,
        "chrono::microseconds must support our timestamp representation on the current arch"
    );

public:
    static constexpr std::size_t buf_size()
    {
        return sizeof m_us_timestamp;
    }

    // getters
    time_point_type timestamp() const
    {
        return time_point_type(std::chrono::microseconds(m_us_timestamp));
    }

    std::uint8_t sender() const { return m_sender; }

    // setters
    // throw on error. Allowed boundries are described above
    void timestamp(time_point_type tp);

    void sender(std::uint8_t id) { m_sender = id; }

    // throw on error. Strong exception safety guaranty
    void serialize(iface::oarchive&);
    void deserialize(iface::iarchive&);

private:
    void timestamp(timestamp_repr_type);

    // msg payload
    // a field to distinguish broadcasters by numeric id It could be a string name,
    // but I suppose 8bit int is OK for the test excercise.
    std::uint8_t m_sender = std::numeric_limits<decltype(m_sender)>::max();
    timestamp_repr_type m_us_timestamp = 0;

    /** @todo for production usage it makes sense to make the protocol extendable
     *  by providing a payload_offset field and ignoring strong datagram length check.
     *  It would allow to keep backward compatibility. Old version clients could work
     *  w/ new version servers' messages, including extended header.
     *  (If we pretend that protobuf libriary doesn't exists, of course)
     */

};

} // namespace proto

