#include "message.hpp"
#include "utils/endian.hpp"

#include <stdexcept>

#include <cstring>


namespace proto {

void msg::timestamp(timestamp_repr_type tp)
{
    static_assert(std::is_unsigned<timestamp_repr_type>::value);
    if (tp > MAX_TP_US_VAL)
        throw std::runtime_error("Incorrect (huge) timepoint");
    m_us_timestamp = tp;
}

void msg::timestamp(time_point_type tp)
{
    namespace sch = std::chrono;
    auto raw_tp = sch::duration_cast<sch::microseconds>(tp.time_since_epoch()).count();
    /// @todo Specific exceptions
    if (raw_tp != static_cast<decltype(raw_tp)>(
        static_cast<timestamp_repr_type>(raw_tp)
    ))
        throw std::runtime_error("Invalid timepoint");

    timestamp(static_cast<timestamp_repr_type>(raw_tp));
}

void msg::serialize(::iface::oarchive& ar)
{
    ar.pack(m_us_timestamp);
}

void msg::deserialize(::iface::iarchive& ar)
{
    timestamp_repr_type tp;
    ar.unpack(tp);
    timestamp(tp);
}

} // namespace proto
