#pragma once

#include "archive.hpp"

#include <memory>
#include <cstddef>


namespace iface {

class broadcaster
{
public:
    virtual void send(oarchive&) = 0;

    virtual std::pair<std::uint8_t/*sender id*/, std::string/*sender address info*/>
    receive(iarchive&) const = 0;

    virtual std::unique_ptr<iarchive> make_iarchive(std::size_t payload_size) const = 0;
    virtual std::unique_ptr<oarchive> make_oarchive() const = 0;

    virtual ~broadcaster() = default;
};

} // namespace iface

