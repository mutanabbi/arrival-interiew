#pragma once

#include <cinttypes>


namespace iface {

class iarchive
{
public:
    class visitor; // fwd

    virtual void reset() = 0;
    virtual void unpack(std::uint8_t& dst) = 0;
    virtual void unpack(std::uint16_t& dst) = 0;
    virtual void unpack(std::uint32_t& dst) = 0;
    virtual void unpack(std::uint64_t& dst) = 0;
    virtual void unpack(void* dst, std::size_t len) = 0;
    virtual void apply(visitor&) = 0;

    virtual ~iarchive() = default;
};

class oarchive
{
public:
    class visitor; // fwd

    virtual void reset() = 0;
    virtual void pack(std::uint8_t src) = 0;
    virtual void pack(std::uint16_t src) = 0;
    virtual void pack(std::uint32_t src) = 0;
    virtual void pack(std::uint64_t src) = 0;
    virtual void pack(const void* src, std::size_t len) = 0;

    virtual void apply(visitor&) = 0;

    virtual ~oarchive() = default;
};

} // namespace iface

