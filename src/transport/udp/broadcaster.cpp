#include "broadcaster.hpp"
#include "../ar_visitor.hpp"
#include "utils/endian.hpp"
#include "utils/utils.hpp"

#include <stdexcept>
#include <string>

#include <arpa/inet.h>

#include <cerrno>
#include <cassert>

#include <unistd.h>
#include <locale.h>
#include <string.h>
#include <iomanip>


namespace {

constexpr unsigned short PORT = __ARRIVAL_BCAST_PORT__;
const char* LOCAL_BROADCAST_ADDR = __ARRIVAL_BCAST_ADDRESS__;

class send_udp_ar_vtor : public ::iface::oarchive::visitor
{
public:
    explicit send_udp_ar_vtor(transport::udp::broadcaster& sender) noexcept
        : m_sender(sender)
    {}
    void visit(transport::udp::oarchive& v) override
    {
        auto [buf, sz] = v.underbuf();
        m_sender.send(buf, sz);
    }

private:
    transport::udp::broadcaster& m_sender;
};

class rcv_udp_ar_vtor : public ::iface::iarchive::visitor
{
public:
    explicit rcv_udp_ar_vtor(const transport::udp::broadcaster& receiver) noexcept
        : m_receiver(receiver)
    {}
    void visit(transport::udp::iarchive& v) override
    {
        auto [buf, sz] = v.underbuf();
        m_info = m_receiver.receive(buf, sz);
        v.unpack_service_data();
        m_sender_id = v.agent_id();
    }
    auto sender() const { return m_sender_id; }
    auto info() const { return m_info; }

private:
    const transport::udp::broadcaster& m_receiver;
    std::string m_info;
    std::uint8_t m_sender_id;
};

} // anonymous namespace


namespace transport::udp {

broadcaster::broadcaster(std::uint8_t agent_id)
  : m_agent_id(agent_id)
  , m_sock(PF_INET, SOCK_DGRAM, 0)
{
    socklen_t val = 1;
    int rc = m_sock.setsockopt(SOL_SOCKET, SO_BROADCAST, &val, sizeof val);
    auto err = errno;
    if (rc == -1)
        throw std::runtime_error(utils::err2str(err));
    assert(rc == 0);

    m_to.sin_family = AF_INET;
    m_to.sin_port = utils::endian::hton(PORT);
    m_to.sin_addr.s_addr = inet_addr(LOCAL_BROADCAST_ADDR);
}

std::unique_ptr<::iface::iarchive> broadcaster::make_iarchive(std::size_t payload_size) const
{
    return std::make_unique<iarchive>(payload_size);
}

std::unique_ptr<::iface::oarchive> broadcaster::make_oarchive() const
{
    return std::make_unique<oarchive>(m_agent_id);
}

void broadcaster::send(::iface::oarchive& oar)
{
    send_udp_ar_vtor v{*this};
    oar.apply(v);
}

std::size_t broadcaster::send(const char* buf, std::size_t sz)
{
    // try to send
    auto rslt = m_sock.sendto(
        buf, sz, MSG_DONTWAIT, reinterpret_cast<const sockaddr*>(&m_to), sizeof m_to
    );

    // send is going to block, so we drop the attempt
    if (-1 == rslt)
    {
        auto err = errno;
        if (err == EWOULDBLOCK || err == EAGAIN || err == EINTR)
            return 0;
        if (err == ENOMEM) // not sure we can propertly handle this
            throw std::runtime_error("No memory");

        assert(!"Can't be here. All other errors must be excluded");
    }

    return rslt;
}

std::pair<std::uint8_t, std::string>
broadcaster::receive(::iface::iarchive& iar) const
{
    rcv_udp_ar_vtor v{*this};
    iar.apply(v);
    return {v.sender(), v.info()};
}

std::string broadcaster::receive(char* buf, std::size_t sz) const
{
    assert("An attempt to receive to zero buffer" && sz > 0);

    // bind if necessary
    if (!m_is_bind)
    {
        auto rc = m_sock.bind(reinterpret_cast<const sockaddr*>(&m_to), sizeof m_to);
        auto err = errno;
        if (rc == -1)
            throw std::runtime_error(utils::err2str(err));
        assert(rc == 0);
        m_is_bind = true;
    }

    sockaddr_in from{};
    socklen_t len = sizeof from;
    int rslt = -1;
    do
    {
        rslt = m_sock.recvfrom(buf, sz, 0, reinterpret_cast<sockaddr*>(&from), &len);
        if (-1 == rslt)
        {
            auto err = errno;
            if (err == ENOMEM) // not sure we can propertly handle this
                throw std::runtime_error("No memory");

            assert(!"Can't be here. All other errors must be excluded");
        }
        assert(rslt >= 0);
    }
    while (static_cast<std::size_t>(rslt) != sz); // drop any inproper size (too small) datagrams
    // It's still possible we received inproper datagram w/ size equal to
    // or greater than buffer size

    return inet_ntoa(from.sin_addr);
}

} // namespace transport::udp
