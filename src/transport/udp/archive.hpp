#pragma once

#include "iface/archive.hpp"

#include <vector>


namespace transport::udp {

class oarchive : public ::iface::oarchive
{
public:
    /// @todo for production quality code it's necessary to count MTU size
    // MTU: 68b-64Kb (IPv4)
    // MTU: 1280b-64Kb (IPv4)
    // UDP header: 8b
    /// @todo bind proto_version on a compile time constant
    explicit oarchive(std::uint8_t id);

    oarchive(const oarchive&) = delete;
    oarchive& operator=(oarchive&) = delete;

    void reset() override;

    void pack(std::uint8_t) override;
    void pack(std::uint16_t) override;
    void pack(std::uint32_t) override;
    void pack(std::uint64_t) override;
    void pack(const void* src, std::size_t len) override;

    void apply(visitor&) override;

    std::pair<const char* /*buf*/, std::size_t/*len*/> underbuf() const;

private:
    template <typename T>
    void inject(T);
    void inject(const void* src, std::size_t slen);

    std::vector<char> m_buf;
    const std::uint8_t m_agent_id;
};

class iarchive : public iface::iarchive
{
public:
    explicit iarchive(std::size_t payload_size);

    iarchive(const iarchive&) = delete;
    iarchive& operator=(iarchive&) = delete;

    void reset() override;

    void unpack(std::uint8_t&) override;
    void unpack(std::uint16_t&) override;
    void unpack(std::uint32_t&) override;
    void unpack(std::uint64_t&) override;
    void unpack(void* dst, std::size_t len) override;

    void apply(visitor&) override;

    std::uint8_t agent_id() const;
    std::pair<char* /*buf*/, std::size_t/*len*/> underbuf();
    void unpack_service_data();

private:
    template <typename T>
    void extract(T&);
    void extract(void* dst, std::size_t len);

    std::vector<char> m_buf;
    std::size_t m_offset = 0;
    const std::size_t m_payload_size;
    std::uint8_t m_agent_id;
};

} // namespace transport udp
