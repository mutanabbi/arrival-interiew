#pragma once

#include <sys/socket.h>
#include <sys/types.h>


// A straitforward RAII wrapper for system socket to provide
// exception safety
//
// see man(2) socket for more detail about method's parameters
// There was no goal to cover all socket related syscalls, just
// neccessery ones.
class sock
{
public:
    sock(int domain, int type, int proto);

    sock(const sock&) = delete;
    sock& operator=(sock&) = delete;

    sock(sock&& rhv) noexcept
        : m_sfd(rhv.m_sfd)
    {
        rhv.m_sfd = -1;
    }
    sock& operator=(sock&& rhv) noexcept;

    int setsockopt(int lvl, int optname, const void* optval, socklen_t optlen) noexcept;
    int connect(const struct sockaddr* addr, socklen_t addrlen) noexcept;
    int bind(const struct sockaddr *addr, socklen_t addrlen) noexcept;
    ssize_t sendto(
        const void *buf, size_t len, int flags,
        const struct sockaddr *dest_addr,
        socklen_t addrlen
    ) noexcept;
    ssize_t recvfrom(
        void *buf, size_t len, int flags,
        struct sockaddr *src_addr,
        socklen_t *addrlen
    ) const noexcept;

    int native() { return m_sfd; }

private:
    void close() noexcept;

    int m_sfd = -1;
};
