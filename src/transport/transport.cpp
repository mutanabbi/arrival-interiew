#include "transport.hpp"

/** @note This isn't technical restriction. Here we just follow a formulation
 *  of the original task. It it will be necessary, it's quiet easy to make
 *  both implementation work together in the app by extending factory method
 *  below
 */
static_assert(
    1 == 0
#if defined(__ARRIVAL_TRANSPORT_CAN__)
    + 1
#endif
#if defined(__ARRIVAL_TRANSPORT_UDP__)
    + 1
#endif
  , "__ARRIVAL_TRANSPORT_CAN__ and __ARRIVAL_TRANSPORT_UDP__ can't be set together"
);

#if defined(__ARRIVAL_TRANSPORT_UDP__)
#  include "udp/broadcaster.hpp"
#endif
#if defined(__ARRIVAL_TRANSPORT_CAN__)
#  include "can/broadcaster.hpp"
#endif

namespace transport {

std::unique_ptr<iface::broadcaster> make_broadcaster(std::uint8_t agent_id)
{
    return
#if defined(__ARRIVAL_TRANSPORT_UDP__)
        std::make_unique<udp::broadcaster>(agent_id);
#endif
#if defined(__ARRIVAL_TRANSPORT_CAN__)
        std::make_unique<can::broadcaster>(agent_id);
#endif
}

} // namespace transport
