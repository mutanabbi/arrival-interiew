#pragma once

#include "iface/archive.hpp"

#include <utility>

#include <linux/can.h>


namespace transport::can {

class oarchive : public ::iface::oarchive
{
public:
    explicit oarchive(std::uint8_t agent_id);

    oarchive(const oarchive&) = delete;
    oarchive& operator=(oarchive&) = delete;

    void reset() override;

    void pack(std::uint8_t src) override;
    void pack(std::uint16_t src) override;
    void pack(std::uint32_t src) override;
    void pack(std::uint64_t src) override;
    void pack(const void* src, std::size_t len) override;

    void apply(visitor&) override;

    std::pair<const char*, std::size_t> underbuf() const;

private:
    template <typename T>
    void inject(T);
    void inject(const void* src, std::size_t slen);

    /** @note We just care about regular CAN frames (8b payload). But the class
     *  can be easely extended to work w/ CAN FD (64b payload)
     */
    can_frame m_frame{};
};


class iarchive : public ::iface::iarchive
{
public:
    // see note about CAN FD above
    explicit iarchive(std::size_t payload_size);

    void reset() override {}

    iarchive(const iarchive&) = delete;
    iarchive& operator=(iarchive&) = delete;

    void unpack(std::uint8_t& dst) override;
    void unpack(std::uint16_t& dst) override;
    void unpack(std::uint32_t& dst) override;
    void unpack(std::uint64_t& dst) override;
    void unpack(void* dst, std::size_t len) override;

    void apply(visitor&) override;

    std::uint8_t agent_id() const;
    std::pair<char*, std::size_t> underbuf();

private:
    template <typename T>
    void extract(T&);
    void extract(void* dst, std::size_t dlen);

    can_frame m_frame;
    std::size_t m_offset = 0;
};

} // namespace transport::can

