#include "broadcaster.hpp"
#include "archive.hpp"
#include "../ar_visitor.hpp"
#include "utils/endian.hpp"
#include "utils/utils.hpp"

#include <stdexcept>

// suppress gcc names clash between `iface` structure fwd from
// net/if.h and same name namespace
namespace {
struct iface;
}
#include <net/if.h>
#include <sys/ioctl.h>

#include <cstring>
#include <cerrno>
#include <cassert>

#include <unistd.h>


namespace {

const char* CAN_IFACE_NAME = __ARRIVAL_CAN_SERVER_IFACE_NAME__;

class send_can_ar_vtor : public ::iface::oarchive::visitor
{
public:
    explicit send_can_ar_vtor(transport::can::broadcaster& sender) noexcept
        : m_sender(sender)
    {}
    void visit(transport::can::oarchive& v) override
    {
        auto [buf, sz] = v.underbuf();
        m_sender.send(buf, sz);
    }

private:
    transport::can::broadcaster& m_sender;
};

class rcv_can_ar_vtor : public ::iface::iarchive::visitor
{
public:
    explicit rcv_can_ar_vtor(const transport::can::broadcaster& receiver) noexcept
        : m_receiver(receiver)
    {}
    void visit(transport::can::iarchive& v) override
    {
        auto [buf, sz] = v.underbuf();
        m_info = m_receiver.receive(buf, sz);
        m_sender_id = v.agent_id();
    }
    auto sender() const { return m_sender_id; }
    auto info() const { return m_info; }

private:
    const transport::can::broadcaster& m_receiver;
    std::string m_info;
    std::uint8_t m_sender_id;
};

} // anonymous namespace


namespace transport::can {

broadcaster::broadcaster(std::uint8_t agent_id)
  : m_agent_id(agent_id)
  , m_sock(PF_CAN, SOCK_RAW, CAN_RAW)
{
    // determine an interface index
    ifreq ifr;
    std::strncpy(ifr.ifr_name, CAN_IFACE_NAME, std::size(ifr.ifr_name));
    int rc = ioctl(m_sock.native(), SIOCGIFINDEX, &ifr);
    auto err = errno;
    /// @todo More error check?
    if (-1 == rc)
        throw std::runtime_error(utils::err2str(err));

    m_to.can_family = AF_CAN;
    m_to.can_ifindex = ifr.ifr_ifindex;
    //m_to.can_ifindex = 0; // all enabled ifaces. Doesn't work for sender
}

std::unique_ptr<::iface::iarchive> broadcaster::make_iarchive(std::size_t payload_size) const
{
    return std::make_unique<iarchive>(payload_size);
}

std::unique_ptr<::iface::oarchive> broadcaster::make_oarchive() const
{
    return std::make_unique<oarchive>(m_agent_id);
}

void broadcaster::send(::iface::oarchive& oar)
{
    send_can_ar_vtor v{*this};
    oar.apply(v);
}

std::size_t broadcaster::send(const char* buf, std::size_t sz)
{
    // try to send
    auto rslt = m_sock.sendto(
        buf, sz, MSG_DONTWAIT, reinterpret_cast<const sockaddr*>(&m_to), sizeof m_to
    );

    // send is going to block, so we drop the attempt
    if (-1 == rslt)
    {
        auto err = errno;
        if (err == EWOULDBLOCK || err == EAGAIN || err == EINTR)
            return 0;
        if (err == ENOMEM) // not sure we can propertly handle this
            throw std::runtime_error("No memory");

        assert(!"Can't be here. All other errors must be excluded");
        throw std::logic_error(utils::err2str(err));
    }

    return rslt;
}

std::pair<std::uint8_t, std::string> broadcaster::receive(::iface::iarchive& iar) const
{
    rcv_can_ar_vtor v{*this};
    iar.apply(v);
    return {v.sender(), v.info()};
}

std::string broadcaster::receive(char* buf, std::size_t sz) const
{
    assert("An attempt to receive to zero buffer" && sz > 0);

    // bind if necessary
    if (!m_is_bind)
    {
        /// @todo Get rid of connect/bind mess
        sockaddr_can to{};
        to.can_family = AF_CAN;
        to.can_ifindex = 0; //  all enabled ifaces
        auto rc = m_sock.bind(reinterpret_cast<const sockaddr*>(&to), sizeof to);
        auto err = errno;
        if (rc == -1)
            throw std::runtime_error(utils::err2str(err));
        m_is_bind = true;
    }

    sockaddr_can from{};
    socklen_t len = sizeof from;
    auto rslt = m_sock.recvfrom(buf, sz, 0, reinterpret_cast<sockaddr*>(&from), &len);
    if (-1 == rslt)
    {
        auto err = errno;
        if (err == ENOMEM) // not sure we can propertly handle this
            throw std::runtime_error("No memory");

        assert(!"Can't be here. All other errors must be excluded");
        throw std::logic_error(utils::err2str(err));
    }

    assert(rslt > 0);
    assert("Socket datagram size doesn't match buffer length" && static_cast<std::size_t>(rslt) == sz);

    {
    ifreq ifr{};
    ifr.ifr_ifindex = from.can_ifindex;
    auto rc = ioctl(m_sock.native(), SIOCGIFNAME, &ifr);
    auto err = errno;
    if (-1 == rc)
        throw std::runtime_error(utils::err2str(err));
    return ifr.ifr_name;
    }
}

} // namespace transport::can

