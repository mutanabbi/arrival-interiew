#include "archive.hpp"
#include "../ar_visitor.hpp"
#include "utils/endian.hpp"

#include <iterator>
#include <stdexcept>

#include <cstring>


namespace transport::can {

void oarchive::inject(const void* src, std::size_t slen)
{
    if (std::size(m_frame.data) - m_frame.can_dlc < slen)
        throw std::runtime_error("Not enouth buffer length"); /// @todo specific exception
    std::memcpy(m_frame.data + m_frame.can_dlc, src, slen);
    m_frame.can_dlc += slen;
}

template <typename T>
void oarchive::inject(T field)
{
    // as soon as I understand CAN socket environment is
    // a local machine, so it isn't crutual to use network ordering,
    // but I use it just for unification
    field = utils::endian::hton(field);
    inject(&field, sizeof field);
}

oarchive::oarchive(std::uint8_t agent_id)
{
    reset();
    m_frame.can_id = agent_id;
}

void oarchive::pack(std::uint8_t src)
{
    inject(src);
}

void oarchive::reset()
{
    m_frame.can_dlc = 0;
}

void oarchive::apply(visitor& v)
{
    v.visit(*this);
}

std::pair<const char*, std::size_t> oarchive::underbuf() const
{
    return {reinterpret_cast<const char*>(&m_frame), sizeof m_frame};
}

void oarchive::pack(std::uint16_t src)
{
    inject(src);
}

void oarchive::pack(std::uint32_t src)
{
    inject(src);
}

void oarchive::pack(std::uint64_t src)
{
    inject(src);
}

void oarchive::pack(const void* src, std::size_t len)
{
    inject(src, len);
}


void iarchive::extract(void* dst, std::size_t dlen)
{
    if (m_frame.can_dlc - m_offset < dlen)
        throw std::runtime_error("Not enouth buffer length"); /// @todo specific exception
    std::memcpy(dst, &m_frame.data[m_offset], dlen);
}

template <typename T>
void iarchive::extract(T& field)
{
    extract(&field, sizeof field);
    field = utils::endian::ntoh(field);
}

iarchive::iarchive(std::size_t payload_size)
{
    assert(
        "CAN frame payload capacity less than necessary"
        && ! (std::size(m_frame.data) < payload_size)
    );
    if (std::size(m_frame.data) < payload_size)
        throw std::logic_error("CAN frame payload capacity less than necessary");
    m_frame.can_dlc = payload_size;
}

void iarchive::apply(visitor& v)
{
    v.visit(*this);
}

std::uint8_t iarchive::agent_id() const
{
    return m_frame.can_id;
}

std::pair<char*, std::size_t> iarchive::underbuf()
{
    return {reinterpret_cast<char*>(&m_frame), sizeof m_frame};
}

void iarchive::unpack(std::uint8_t& dst)
{
    extract(dst);
}

void iarchive::unpack(std::uint16_t& dst)
{
    extract(dst);
}

void iarchive::unpack(std::uint32_t& dst)
{
    extract(dst);
}

void iarchive::unpack(std::uint64_t& dst)
{
    extract(dst);
}

void iarchive::unpack(void* dst, std::size_t len)
{
    extract(dst, len);
}

} // namespace transport::can

