message(STATUS "CAN transport is chosen (TRANSPORT=can)")
message(STATUS "CAN_IFACE=`${CAN_IFACE}'")
if(NOT DEFINED CAN_IFACE)
    message(FATAL_ERROR "Please, set proper CAN interface name w/ `-DCAN_IFACE=<name>' option") 
endif()

target_sources(transport PRIVATE
    broadcaster.cpp
    archive.cpp
)

set_property(
    TARGET transport
    PROPERTY COMPILE_DEFINITIONS
    __ARRIVAL_TRANSPORT_CAN__=1
    __ARRIVAL_CAN_SERVER_IFACE_NAME__="${CAN_IFACE}"
)
