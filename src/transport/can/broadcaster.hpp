#pragma once

#include "iface/broadcaster.hpp"
#include "../sock.hpp"

#include <linux/can.h>


namespace transport::can {

class broadcaster : public ::iface::broadcaster
{
public:
    explicit broadcaster(std::uint8_t agent_id);

    broadcaster(const broadcaster&) = delete;
    broadcaster& operator=(broadcaster&) = delete;

    broadcaster(broadcaster&&) = default;
    broadcaster& operator=(broadcaster&&) = default;

    // polymorphic interface
    void send(::iface::oarchive&) override;

    std::pair<std::uint8_t/*sender id*/, std::string/*sender address info*/>
    receive(::iface::iarchive&) const override;

    std::unique_ptr<::iface::iarchive> make_iarchive(std::size_t payload_size) const override;
    std::unique_ptr<::iface::oarchive> make_oarchive() const override;

    // transport lib level interface
    /// @todo may be it's good idea to split send() try_send() methods
    std::size_t send(const char* buf, std::size_t sz);
    std::string/*sender info*/ receive(char* buf, std::size_t sz) const;

private:

    std::uint8_t m_agent_id;
    /// @todo avoid mutable
    sockaddr_can m_to{};
    mutable sock m_sock;
    mutable bool m_is_bind = false;
};

} // namespace transport::can

