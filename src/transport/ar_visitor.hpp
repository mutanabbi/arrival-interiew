#pragma once

#include "udp/archive.hpp"
#include "can/archive.hpp"

#include <stdexcept>
#include <cassert>


namespace iface {

class iarchive::visitor
{
public:
    virtual void visit(iarchive&)
    {
        assert(!"Invalid visitor");
        throw std::logic_error("Invalid visitor");
    }
    virtual void visit(transport::udp::iarchive& v) { visit(static_cast<iarchive&>(v)); }
    virtual void visit(transport::can::iarchive& v) { visit(static_cast<iarchive&>(v)); }

    virtual ~visitor() = default;
};

class oarchive::visitor
{
public:
    virtual void visit(oarchive&)
    {
        assert(!"Invalid visitor");
        throw std::logic_error("Invalid visitor");
    }
    virtual void visit(transport::udp::oarchive& v) { visit(static_cast<oarchive&>(v)); }
    virtual void visit(transport::can::oarchive& v) { visit(static_cast<oarchive&>(v)); }

    virtual ~visitor() = default;
};

} // namespace iface

