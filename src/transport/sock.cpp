#include "sock.hpp"
#include "utils/utils.hpp"

#include <stdexcept>

#include <cassert>

#include <unistd.h>


sock::sock(int domain, int type, int proto)
{
    m_sfd = ::socket(domain, type, proto);
    auto err = errno;
    if (-1 == m_sfd)
        throw std::runtime_error(utils::err2str(err));
}

sock& sock::operator=(sock&& rhv) noexcept
{
    if (&rhv != this)
    {
        this->close();
        m_sfd = rhv.m_sfd;
        rhv.m_sfd = -1;
    }
    return *this;
}

int sock::setsockopt(int lvl, int optname, const void* optval, socklen_t optlen) noexcept
{
    return ::setsockopt(m_sfd, lvl, optname, optval, sizeof optlen);
}

int sock::connect(const struct sockaddr* addr, socklen_t addrlen) noexcept
{
    return ::connect(m_sfd, addr, addrlen);
}

int sock::bind(const struct sockaddr *addr, socklen_t addrlen) noexcept
{
    return ::bind(m_sfd, addr, addrlen);
}

ssize_t sock::sendto(
    const void *buf,
    size_t len,
    int flags,
    const struct sockaddr *dest_addr,
    socklen_t addrlen
) noexcept
{
    return ::sendto(m_sfd, buf, len, flags, dest_addr, addrlen);
}

ssize_t sock::recvfrom(
    void *buf,
    size_t len,
    int flags,
    struct sockaddr *src_addr,
    socklen_t *addrlen
) const noexcept
{
    return ::recvfrom(m_sfd, buf, len, flags, src_addr, addrlen);
}

void sock::close() noexcept
{
    if (-1 != m_sfd)
    {
        int rc = ::close(m_sfd);
        // resource releasing methods must no throw and be allways successful by nature, so
        // we don't propagate possible unknown IO errors at NDEBUG mode. A wrong file
        // descriptor is obviously logical error, so we assert these cases.
        // Though we completely ignore signal interruptions - that's user's responsibility
        // to use proper signal handling/masking model.
        assert(-1 != rc || errno == EINTR);
        (void)rc; // suppress unused var warning
    }
}
