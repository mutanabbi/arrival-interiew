#pragma once

#include "iface/broadcaster.hpp"
#include <memory>

namespace transport {

std::unique_ptr<iface::broadcaster> make_broadcaster(std::uint8_t agent_id);

} // namespace transport

