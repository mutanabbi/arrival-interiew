#include "utils.hpp"

#include <stdexcept>

#include <string.h>
#include <locale.h>
#include <cerrno>


namespace {

class Locale
{
public:
    Locale()
    {
        m_lcl = newlocale(
            LC_CTYPE_MASK |
            LC_NUMERIC_MASK |
            LC_TIME_MASK |
            LC_COLLATE_MASK |
            LC_MONETARY_MASK |
            LC_MESSAGES_MASK,
            "",
            locale_t(0)
        );
        if (0 == m_lcl && ENOMEM == errno)
            throw std::runtime_error("ENOMEM");
    }
    Locale(const Locale&) = delete;
    Locale& operator=(Locale&) = delete;
    const char* err2str(int errnum) const
    {
        return strerror_l(errnum, m_lcl);
    }
    ~Locale() { freelocale(m_lcl); }

private:
    locale_t m_lcl{0};
};

} // anonymous namespace

namespace utils {

const char* err2str(int errnum)
{
    static const Locale s_lcl;
    return s_lcl.err2str(errnum);
};

} // namespace utils
