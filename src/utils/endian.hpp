#pragma once

#include <arpa/inet.h>

#include <cinttypes>


namespace utils::endian {

inline std::uint8_t hton(std::uint8_t v)
{
    return v;
}

inline std::uint16_t hton(std::uint16_t v)
{
    return htons(v);
}

inline std::uint32_t hton(std::uint32_t v)
{
    return htonl(v);
}

inline std::uint64_t hton(std::uint64_t v)
{
    return static_cast<std::uint64_t>(
            hton(static_cast<std::uint32_t>(v))
        ) << 32 | static_cast<std::uint64_t>(
            hton(static_cast<std::uint32_t>(v >> 32))
        );
}

inline std::uint8_t ntoh(std::uint8_t v)
{
    return v;
}

inline std::uint16_t ntoh(std::uint16_t v)
{
    return ntohs(v);
}

inline std::uint32_t ntoh(std::uint32_t v)
{
    return ntohl(v);
}

inline std::uint64_t ntoh(std::uint64_t v)
{
    return static_cast<std::uint64_t>(
            ntoh(static_cast<std::uint32_t>(v))
        ) << 32 | static_cast<std::uint64_t>(
            ntoh(static_cast<std::uint32_t>(v >> 32))
        );
}

}                                                           // namespace endian, utils

