#include "transport/transport.hpp"
#include "proto/message.hpp"

#include <iostream>
#include <filesystem>
#include <string>
#include <chrono>
#include <thread>

#include <cassert>


int main(int argc, char* argv[])
{
    // Dirty in-place CLI parsing. Good enought for this simple example, but
    // must be redesigned for production quality code
    if (argc < 2)
    {
        std::cout << "Usage: " << std::filesystem::path(argv[0]).filename().c_str()
            << " <agent_id>" << std::endl;
        return -1;

    }
    std::uint8_t agent_id = 0;
    try
    {
        unsigned long tmp = std::stoul(argv[1], nullptr, 10);
        if (tmp > std::numeric_limits<decltype(agent_id)>::max())
            throw std::logic_error("Value too big");
        agent_id = tmp;
    }
    catch(const std::exception&)
    {
        std::cerr << "Invalid agent_id" << std::endl;
        return -2;
    }

    auto bcaster = transport::make_broadcaster(agent_id);

    const auto SLEEP_DUR = std::chrono::milliseconds(100);
    proto::msg m;
    auto ar = bcaster->make_oarchive();

    while (true)
    {
        m.timestamp(std::chrono::system_clock::now());
        ar->reset();
        m.serialize(*ar);
        bcaster->send(*ar);
        std::this_thread::sleep_until(m.timestamp() + SLEEP_DUR);
    }

    return 0;
}
